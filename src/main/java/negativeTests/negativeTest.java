package negativeTests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.JsonReader;
import java.io.FileNotFoundException;

public class negativeTest extends BaseNegativeTest {

    private final String PATH = "/Users/shalevzrihen/IdeaProjects/new/src/main/java/json_files/login.json";

    @DataProvider(name = "login")
    public Object[][] passData() throws FileNotFoundException {
        return JsonReader.getData(PATH,"login data",2,2);
    }

    @Test(dataProvider = "login")
    public void login_negative(String userName, String password){
        LoginPage lg = new LoginPage(driver,wait);
        lg.negative_login(userName, password);
        Assert.assertEquals(1,2 );
        lg.validateLogin();
    }
}

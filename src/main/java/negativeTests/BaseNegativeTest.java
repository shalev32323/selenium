package negativeTests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import utils.BaseTest;

public class BaseNegativeTest extends BaseTest {

    @BeforeMethod
    @Override
    public void setUp(){
        super.setUp();
    }

    @AfterMethod
    @Override
    public void terminate(){
        super.terminate();
    }
}

package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class LoginPage extends BasePage{

    //Elements
    @FindBy(css = "input")
    private List<WebElement> inputs;

    @FindBy(css =".svg-inline--fa.fa-times.fa-w-11")
    private WebElement x_button;

    @FindBy(css = "[data-test='error'")
    private WebElement error_massage;

    //Constructor
    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }


    public void negative_login(String userName, String password){
        this.sendKeys(this.inputs.get(0), userName);
        this.sendKeys(this.inputs.get(1), password);
        this.click(this.inputs.get(2),"error massage");
//        this.logPassValues(userName,password);
        this.assert_visible(this.error_massage);
    }

    public void positive_login(String userName, String password){
        this.sendKeys(this.inputs.get(0), userName);
        this.sendKeys(this.inputs.get(1), password);
        this.click(this.inputs.get(2),"error massage");
//        this.logPassValues(userName,password);
        this.assert_visible(this.error_massage);
    }


    public void validateLogin(){
        try {

            this.inputs.get(0).clear();
            this.inputs.get(1).clear();
            this.click(this.x_button, "x button");
        }
        catch (Exception e){
            this.driver.navigate().back();
        }
    }
}

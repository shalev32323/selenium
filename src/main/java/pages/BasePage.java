package pages;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import utils.TestListener;
import java.util.ArrayList;
import java.util.Arrays;


public class BasePage extends TestListener {

    protected WebDriver driver;
    protected WebDriverWait wait;

    // Constructor
    public BasePage(WebDriver driver, WebDriverWait wait) {
        this.wait = wait;
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    
    public void sendKeys(WebElement element,String value){
        this.wait.until(ExpectedConditions.visibilityOf(element)).sendKeys(value);
        test.log(Status.PASS, "insert  " + value);
    }

    public void click(WebElement element,String name){
        this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
        test.log(Status.PASS, "click  " + name);
    }

    public void OpenNewTab(String url) {
        JavascriptExecutor js = (JavascriptExecutor)this.driver;
        js.executeScript("window.open('" + url + "');");
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size() -1));
        test.log(Status.PASS, "open a new tab" );
  }

    public void switchToParentTab() {
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
        test.log(Status.PASS, "go back to parent tab" );
    }

    public void switchFrames(){
        this.driver.switchTo().frame(1);
        test.log(Status.PASS, "switch frame" );
    }

    public void switchToParentFrame(){
        this.driver.switchTo().parentFrame();
        test.log(Status.PASS, "switch to parent frame" );
    }

    public void acceptAlert(){
        this.driver.switchTo().alert().accept();
        test.log(Status.PASS, "accept alert" );
    }

    public void dismissAlert(){
        this.driver.switchTo().alert().dismiss();
        test.log(Status.PASS, "dismiss alert" );
    }

    public void assert_visible(WebElement element){
        Assert.assertTrue(element.isDisplayed());
        test.log(Status.PASS, "check if the element is visible" );
    }

    public void logPassValues(String ...values){
        test.log(Status.PASS, Arrays.asList(values).toString());
    }
}



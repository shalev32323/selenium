package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import reports.ScreenShotGenerate;
import java.io.IOException;


public class TestListener implements ITestListener {

    private ExtentSparkReporter spark = new ExtentSparkReporter(System.getProperty("user.dir") + "/src/main/java/reports/Report.html");
    private ExtentReports extent = new ExtentReports();
    public static ExtentTest test;

    @Override
    public synchronized void onTestStart(ITestResult result) {
        test = extent.createTest(result.getName());
    }

    @Override
    public synchronized  void onTestSuccess(ITestResult result) {
        test.log(Status.PASS,result.getName());
    }

    @Override
    public synchronized  void onTestFailure(ITestResult result) {
        test.fail(result.getThrowable(),
                MediaEntityBuilder.createScreenCaptureFromPath("./screenshots/"
                        + ScreenShotGenerate.takeScreenShot(result.getName() + "_")).build());

    }

    @Override
    public synchronized  void onTestSkipped(ITestResult result) {
        test.skip(result.getThrowable());
    }

    @Override
    public  synchronized void onTestFailedButWithinSuccessPercentage(ITestResult result) {
    }

    @Override
    public  synchronized void onStart(ITestContext context) {
        spark.config().setDocumentTitle("Report");
        spark.config().setReportName("shalev");
        spark.config().setTheme(Theme.DARK);
        extent.attachReporter(spark);
    }

    @Override
    public synchronized  void onFinish(ITestContext context) {
        extent.flush();
    }
}


package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BaseTest {
    {
        System.setProperty("webdriver.chrome.driver", "/Users/shalevzrihen/Documents/selenium/chromedriver");
    }

    public static WebDriver driver;
    public static WebDriverWait wait;


    public void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        System.out.println("creating driver " + getClass().getName());
    }

    public void terminate() {
        driver.quit();
    }
}


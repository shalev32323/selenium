package positiveTests;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import utils.BaseTest;

public class BasePositiveTest extends BaseTest {

    static final String BASE_URL = "https://www.google.co.il/?client=safari&channel=mac_bm";

    @BeforeTest
    @Override
    public void setUp(){
        super.setUp();
    }

    @BeforeClass
    void open() {
        System.out.println("Opening " + getClass().getName());
        driver.get(BASE_URL);
        driver.manage().window().maximize();
    }

    @AfterTest
    @Override
    public void terminate(){
        super.terminate();
    }
}
